import argparse
import glob

from includes.DataManager import DataManager
from includes.Analysis import Analysis
from includes.FileHelper import FileHelper

FRAMES_IN_CHUNK = 1000

# argparse
# TODO parse map
parser = argparse.ArgumentParser(description="Parse arguments from from parameters")
parser.add_argument('-f', '--folder', default="../calibration_-ak2/data/Fe55/", help='Working folder')
parser.add_argument('-s', '--source', default="Pu", help='Source')
parser.add_argument('-b', '--background', action='store', help='Enable background with putting the path to background '
                                                               'data')
parser.add_argument('-m', '--map', default="matrix", help='Pixels map')
args = parser.parse_args()

# initialization for all includes
dm = DataManager()
an = Analysis()
fm = FileHelper()

dm.set_n_frames_in_chunck(1000)  # count of frames readied per step

# flag for background
is_background = False
if args.background is not None:
    # background data parse
    dm.load_background_data_from_file(str(args.background))
    is_background = True

# Set work dir and create output
fm.Set_work_dir_path(args.folder)
fm.Create_out_put_dir()

# Parse data from file with file buffering and dividing into chunks
for file in glob.glob(args.folder + "/" + "*.dat"):
    fm.Set_work_file_path(file)
    fm.Set_out_put_file_name()
    dm.adc_array = []  # reinit array for each file
    with open(file, "rb", buffering=2000000) as f:
        raw_data = dm.generate_chunks(f, dm.n_frames)  # read n_frames per step

        # Read data and ADC depends on user preferences pixel, column, whole matrix or a few columns
        dm.load_frames(raw_data, matrix=True, background=True)

        # creat hist and save to csv
        current_histo_file_name = fm.Get_out_put_histo_path() \
                                  + fm.Get_out_put_file_name()

        dm.hist_and_peaks_to_csv(current_histo_file_name)

# Read all histo, create spectrumm with calculated transfer function
an.find_trans_func(args.folder, fm, deg=1)
an.Calibration(args.folder, an.trans_func, fm)
