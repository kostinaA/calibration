::
::
::
SET PYTHON_DIR="C:\Users\kosti\AppData\Local\Programs\Python\Python310\"
SETX PATH "%PATH%;%PATH%\..;%PYTHON_DIR%"
::
:: update pip if necessary
::
%PYTHON_DIR%\python.exe -m pip install -U pip
::
:: install dependency
::
%PYTHON_DIR%\python.exe -m pip install matplotlib
%PYTHON_DIR%\python.exe -m pip install numpy
%PYTHON_DIR%\python.exe -m pip install pandas
%PYTHON_DIR%\python.exe -m pip install seaborn
%PYTHON_DIR%\python.exe -m pip install scipy
%PYTHON_DIR%\python.exe -m pip install argparse

