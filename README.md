# Calibration

This is one of the options for chip calibration (pixel by pixel)  
## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install all necessary libraries

```bash
pip install numpy
```
In case you use Windows, you run use [install.bat](install.bat). 
Make sure that you have Python and the right path to the executed file.

## Usage
Parameters are required to use the script:
* '-f', '--folder', default="../calibration_-ak2/data/Fe55/"
* '-s', '--source'. Will be parsed from file name by default.
* '-b', '--background', action='store', help='Enable background with putting the path to background '
 
The example [start.bat](start.bat):
``` bash
python.exe index.py -s Fe -f {Folder with data} -b {Folder with background data}
```

Data filename and format are important to automatic analysis and saving results. 

``` bash
{CHIP-NAME}_{bias}_{Source}_.dat
```

### TESTING 
Fake spectrums are putted in folder [Test](Test) to help integrated testing.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.


### TODO
- [ ] Map is not implemented yet. { have to be settled manually  into DataManager.load_frames() }
 - [ ] function get_column_adc could be deleted and implement into get_columns_adc
``` python
 dm.load_frames(matrix=True)
 dm.load_frames(pixel=[0,0])
 dm.load_frames(columns=[1,2,3,4])
 dm.load_frames(column = 4)

```