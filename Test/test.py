import unittest
import numpy as np
from includes.Analysis import Analysis


class DataManagerTest(unittest.TestCase):

    def test_peaks_info_saving(self):
        '''
        save peaks info to file
        Its bad example of unit test
        '''
        peaks_info = []
        peaks_index = [0, 1, 2, 3]
        bin_edge = [100, 200, 300, 500, 700, 900]
        hist = [0, 52, 35, 25, 47, 98]
        for index in peaks_index:
            if hist[index] != 0:
                pair = (bin_edge[index], hist[index])
                peaks_info.append(pair)
        np.savetxt("test.csv", peaks_info, delimiter=",")

        self.assertEqual(len(peaks_info), 3, "Should be 3")

    def test_sum_tuple(self):
        self.assertEqual(sum((1, 2, 2)), 6, "Should be 6")


class AnalysisTest(unittest.TestCase):
    an = Analysis()

    def test_peaks_count(self):
        real = [0, 2]
        measured = [1, 2, 3, 4]
        result = self.an.peaks_count(real, measured)
        self.assertEqual(result, 2, "Should be 2")

        real = [0, 2, 1]
        measured = [1, 2]
        result = self.an.peaks_count(real, measured)
        self.assertEqual(result, 2, "Should be 2")


if __name__ == '__main__':
    unittest.main()
