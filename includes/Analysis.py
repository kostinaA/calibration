import glob
import os
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from includes.SourcesInfo import SourcesInfo
from includes.FileHelper import FileHelper
from sympy import S, symbols, printing


class Analysis:
    NUMBER_OF_SAMPLES_FOR_POLYNOMIAL = 10
    MIN_ENERGY_KEV = 4
    MAX_ENERGY_KEV = 25

    def __init__(self):
        self.path = ""
        self.peaks = []
        self.calData = []
        self.sinfo = SourcesInfo()
        self.peaksInfo = []
        self.peaksCount = 0
        self.fileHelper = FileHelper()
        self.trans_func = ""

    @staticmethod
    def peaks_count(energy_peaks, adc_peaks):
        """
        Peaks count depends on real measure and theoretical information about spectrum
        :param adc_peaks: array
        required array of peaks from real measurements
        :param energy_peaks: array
        required array, theoretical info
        :return: int
        """
        peaks_count = len(adc_peaks)
        if peaks_count < len(energy_peaks):
            peaks_count = len(adc_peaks)
        else:
            peaks_count = len(energy_peaks)
        return peaks_count

    def find_trans_func(self, path, filehelper, deg=1):
        """
        Peak file processing. Search for a transfer function(linear, quadratic and cubic).
        There is manual selection of the used function for calibration
        :param
        path: string, path to working folder
        :param
        filehelper: object
        """
        total_adc_peaks = []
        real_energy_peaks = []

        # peaks files processing
        for file in glob.glob(path + "output/histo//" + "*_peaks.csv"):

            # Get source and chip from file name
            file_path_info = os.path.split(file)
            chip_name, _, source_name, _ = file_path_info[1].split("_")

            self.sinfo.Source = source_name

            df = pd.read_csv(file, usecols=[0, 1], names=['ADC', 'FREQ'], header=None)

            # peaks count depends on measurements
            peaks_count = self.peaks_count(self.sinfo.GetSourceInfo(), df['ADC'])

            for adc in df['ADC'][0:peaks_count]:
                total_adc_peaks.append(adc)

            for energy in self.sinfo.GetSourceInfo()[0:peaks_count]:
                real_energy_peaks.append(energy)

        # get polynomial fit and function
        xp = np.linspace(min(total_adc_peaks), max(total_adc_peaks), self.NUMBER_OF_SAMPLES_FOR_POLYNOMIAL)
        polynomial_fit = np.polyfit(total_adc_peaks, real_energy_peaks, deg)

        x = symbols("x")

        self.trans_func = sum(S("{:6.2f}".format(v)) * x ** i for i, v in enumerate(polynomial_fit[::-1]))

        eq_latex = printing.latex(self.trans_func)

        # plot and save graph
        plt.plot(total_adc_peaks, real_energy_peaks, 'o')
        plt.plot(xp, np.polyval(polynomial_fit, xp), 'b--', label="${}$".format(eq_latex))
        plt.ylim(self.MIN_ENERGY_KEV, self.MAX_ENERGY_KEV)
        plt.xlabel('ADC')
        plt.ylabel('Energy[keV]')
        plt.title("Regression plot")
        plt.legend(fontsize="small")
        current_output_file = filehelper.Get_out_put_plot_path() + filehelper.Get_out_put_file_name()
        plt.savefig(current_output_file + "Total_regresplot.jpg", dpi=1200)
        plt.show()
        plt.close()

    def Calibration(self, path, transfer_func, filehelper):
        labels = []
        # replacing x with df['ADC'] for future calculation
        energy_eq = str.replace(str(transfer_func), "x", "df['ADC']")
        eq_latex = printing.latex(transfer_func)

        for file in glob.glob(path + "output/histo//" + "*histo.csv"):
            # Get source and chip from file name
            file_path_info = os.path.split(file)
            chip_name, _, source_name, _ = file_path_info[1].split("_")

            # Append labels for legend
            labels.append(source_name)

            # Read histo from file to Dataframe
            df = pd.read_csv(file, usecols=[0, 1], names=['ADC', 'FREQ'], header=None)
            energy_bin_edges = abs(eval(energy_eq))
            plt.bar(energy_bin_edges, df['FREQ'])

        plt.xlim(self.MIN_ENERGY_KEV, self.MAX_ENERGY_KEV)
        plt.legend(labels)
        plt.ylabel('count')
        plt.xlabel('Energy[keV]')
        text = "y = " + eq_latex
        plt.title(text)
        plt.suptitle("Calibrated")

        current_output_file = filehelper.Get_out_put_plot_path() + filehelper.Get_out_put_file_name()
        plt.savefig(current_output_file + "Calibrate_spectrum.jpg", dpi=1200)
        plt.show()