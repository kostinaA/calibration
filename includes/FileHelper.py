# Author: Anhelina Kostina <anhelina.kostina@fjfi.cvut.cz>.

import os

'''
The class contains the following public function:
    - Create_out_put_dir
    - Get_out_put_histo_path
    - Get_out_put_plot_path
    - Get_out_put_file_name
    - Set_work_dir_path
    - Set_work_file_path
    - Set_out_put_file_name
'''


class FileHelper:
    def __init__(self):
        self.work_dir_path = ""
        self.out_put_dir_path = ""
        self.output_dir_path = ""
        self.output_file_name = ""
        self.work_file_path = ""
        self.output_histo_path = ""
        self.output_plots_path = ""

    # Create output directory if it's not exist
    def Create_out_put_dir(self):
        self.out_put_dir_path = self.work_dir_path + '/' + 'output' + '/'

        if not os.path.exists(self.out_put_dir_path):
            os.makedirs(self.out_put_dir_path)

        self.output_histo_path = self.out_put_dir_path + '/' + 'histo' + '/'
        if not os.path.exists(self.output_histo_path):
            os.makedirs(self.output_histo_path)

        self.output_plots_path = self.out_put_dir_path + '/' + 'plots' + '/'
        if not os.path.exists(self.output_plots_path):
            os.makedirs(self.output_plots_path)

    def Get_out_put_histo_path(self):
        return self.output_histo_path

    def Get_out_put_plot_path(self):
        return self.output_plots_path

    def Get_out_put_file_name(self):
        return self.output_file_name

    def Set_work_dir_path(self, path):
        self.work_dir_path = path

    def Set_work_file_path(self, file_path):
        self.work_file_path = file_path

    '''
    Set output file name  with info about used chip, source and bias.
    It is important to make the name of the file according to the example:
        XCHIP-03-AFN-SXR_120Vbias_Pu_10ms-30000frames.dat
    '''

    def Set_out_put_file_name(self, file_name=None):
        if file_name is not None:
            self.output_file_name = file_name
        else:
            file_info_path = self.work_file_path.split('\\')[-1]
            chip_name = file_info_path.split("_")[0]
            bias_info = file_info_path.split("_")[1]
            source_name = file_info_path.split("_")[2]
            self.output_file_name = chip_name + "_" + bias_info + "_" + source_name
