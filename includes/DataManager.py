# Author: Anhelina Kostina <anhelina.kostina@fjfi.cvut.cz>.
import os
import matplotlib.pyplot as plt
import numpy as np
import gc
import glob
import scipy.signal
from includes.Peak import Peak


class DataManager:
    BACKGROUND_THRESHOLD = 10
    ROWS_COUNT = COLUMNS_COUNT = 64  # detector size
    ONE_FRAME_SIZE_IN_BYTE = 8192

    def __init__(self):
        self.path = ""
        self.file_name = ""

        self.path_dark = ""
        self.file_name_dark = ""

        self.n_frames = 0
        self.n_frames_dark = 0

        self.frames_raw = np.zeros((self.n_frames,
                                    self.ROWS_COUNT,
                                    self.COLUMNS_COUNT),
                                   dtype='uint8')
        self.frames_raw_background = np.zeros((self.n_frames,
                                               self.ROWS_COUNT,
                                               self.COLUMNS_COUNT),
                                              dtype='uint8')

        self.frames_annulated_background = np.zeros((self.n_frames,
                                                     self.ROWS_COUNT,
                                                     self.COLUMNS_COUNT),
                                                    dtype='uint8')
        self.frame_offset = np.zeros((self.ROWS_COUNT, self.COLUMNS_COUNT), dtype='uint8')
        self.adc = []
        self.total_n_frames = 0
        self.size = 0
        self.adc_array = []
        self.pk = Peak(0)

    def set_n_frames_in_chunck(self, n_frames):
        self.n_frames = n_frames

    def set_size(self, n_frames):
        self.size = n_frames * self.ROWS_COUNT * self.COLUMNS_COUNT
        return self.size

    def set_total_n_frames(self, file):
        """
        :param file: path
        :return: total cout of frames in file
        """
        self.total_n_frames = int(os.stat(file).st_size / self.ONE_FRAME_SIZE_IN_BYTE)
        return self.total_n_frames

    def annulate_background(self, raw_data, threshold):
        """

        :param raw_data: converted data from file
        :param threshold: integer,
        differences
        :return:
        """
        self.frames_annulated_background = np.zeros((self.n_frames, self.ROWS_COUNT, self.COLUMNS_COUNT), dtype='uint8')
        raw_data[raw_data - self.frames_raw_background < threshold] = 0
        self.frames_annulated_background = raw_data

    def compute_offset_frame(self, frames_dark):
        """
        :param frames_dark: original data from background file
        :return:
        """
        n_frames_dark = len(frames_dark)
        for i in range(0, n_frames_dark):
            self.frame_offset = self.frame_offset + frames_dark[i]
        self.frame_offset = self.frame_offset // n_frames_dark

    def get_n_frames_dark(self, filepath):
        self.n_frames_dark = int(os.stat(filepath).st_size / 8192)
        return self.n_frames_dark

    def get_frame(self, frame_index):
        """

        :param frame_index: int
        :return: one frame
        """
        return self.frames_raw[frame_index, :, :]

    def generate_chunks(self, input_file, frames_in_chunk):
        # generete chunk meanwhile reading 16bit file
        while (True):
            yield np.fromfile(input_file, count=frames_in_chunk * 64 * 64, dtype=np.uint16)

    def load_frames(self, raw_data, pixel=None, column=None, matrix=False, columns=None, background=False):
        """
        Read data and ADC depends on user preferences pixel, column, whole matrix or a few columns

        Parameters
        ----------
        raw_data : sequence, data from file
        pixel: ndarray
            Required pixel address
        column: int
            Required column address
        matrix: boolean
            Required flag to use whole matrix
        columns: ndarray
            Required columns addresses
        background: boolean
            Required flag to deduct background from all measurements
        """
        for chunk in raw_data:

            if len(chunk) == 0:
                break

            if len(chunk) < self.n_frames * 64 * 64:
                break

            super_frame = chunk.reshape(self.n_frames * 64, 64)
            self.frames_raw = np.zeros((self.n_frames, 64, 64))
            #
            for i in range(0, self.n_frames):
                frame = super_frame[i * 64:(i + 1) * 64, 0:64]
                frame1 = frame >> 8
                frame2 = frame << 8
                frame = frame1 + frame2
                self.frames_raw[i] = frame
            if background is True:
                # anulate background with or without threshold
                self.annulate_background(self.frames_raw, self.BACKGROUND_THRESHOLD)
                self.frames_raw = self.frames_annulated_background

            if pixel is not None:
                self.get_pixel_adc(pixel[0], pixel[1])  # only  one pixel
            if matrix is True:

                for adcValue in self.iterThrough(self.frames_raw):
                    self.adc_array.append(int(adcValue))
            if column is not None:
                self.get_column_adc(int(column))  # get adc for specific column
            if columns is not None:
                self.get_columns_adc(columns)  # get adc for specific column

        '''
        Its not necessary thing but can help PC a bit for the next step
        '''
        # memory's clearing
        self.frames_raw = []
        super_frame = []
        self.frames_annulated_background = []
        gc.collect()

    def get_pixel_adc(self, current_row, сurrent_column):
        for frame in range(0, self.n_frames):
            adc_value = self.frames_raw[frame][int(current_row)][int(сurrent_column)]
            self.adc_array.append(int(adc_value))

    # average ADC of all pix in the matrix
    def get_matrix_adc(self):
        matrix_size = 64 * 64
        for frame in range(0, self.n_frames):
            adc_value = np.array(self.frames_raw[frame]).sum() / matrix_size
            self.adc_array.append(int(adc_value))

    def get_column_adc(self, current_column):
        '''
        Adc array from all pixels from one column
        :param current_column: integer

        '''
        rows = np.arange(0, 64, 1)
        for frame in range(0, self.n_frames):
            for index_row in rows:
                adc_pix = self.frames_raw[frame][int(index_row)][int(current_column)]
                self.adc_array.append(int(adc_pix))

    def get_columns_adc(self, columns_array):
        """
        Adc array from all pixels from list of columns
        :param columns_array: ndarray
        list of columns
        """
        rows = np.arange(0, 64, 1)
        for frame in range(0, self.n_frames):
            for index_column in columns_array:
                for index_row in rows:
                    adc_pix = self.frames_raw[frame][int(index_row)][int(index_column)]
                    self.adc_array.append(int(adc_pix))

    # helper to get adc value from N-dimensional frame
    def iterThrough(self, lists):
        '''
        iteration through all elements in N-dimensional
        :param lists: ndarray
        :return:
        '''
        if not hasattr(lists[0], '__iter__'):
            for val in lists:
                yield val
        else:
            for l in lists:
                for val in self.iterThrough(l):
                    yield val

    def load_background_frames(self, raw_data):
        """
        Read data and ADC depends on user preferences pixel, column, whole matrix or a few columns

        Parameters
        ----------
        raw_data : sequence, data from background file

        """
        for chunk in raw_data:

            if len(chunk) == 0:
                break

            if len(chunk) < self.n_frames * 64 * 64:
                break

            super_frame = chunk.reshape(self.n_frames * 64, 64)
            self.frames_raw_background = np.zeros((self.n_frames, 64, 64))
            #
            for i in range(0, self.n_frames):
                frame = super_frame[i * 64:(i + 1) * 64, 0:64]
                frame1 = frame >> 8
                frame2 = frame << 8
                frame = frame1 + frame2
                self.frames_raw_background[i] = frame

    # TODO: Bad naming for function
    def load_background_data_from_file(self, path):
        for file in glob.glob(path + "/" + "*.dat"):
            with open(file, "rb") as f:
                raw_data = self.generate_chunks(f, self.n_frames)  # read n_frames per step

                # Read data and ADC depends on user preferences pixel, column, whole matrix or a few columns
                self.load_background_frames(raw_data)
        # compute offset frame
        self.compute_offset_frame(self.frames_raw_background)

    def files_processing(self, path, filehelper):
        """
        Open all files with extension *.dat by chunks, load frames and create a hist for each
        :param path: string,
         path to folder
        :param filehelper: object

        """
        for file in glob.glob(path + "/" + "*.dat"):
            filehelper.Set_work_file_path(file)
            filehelper.Set_out_put_file_name()
            self.adc_array = []  # reinit array for each file
            with open(file, "rb", buffering=2000000) as f:
                raw_data = self.generate_chunks(f, self.n_frames)  # read n_frames per step

                # Read data and ADC depends on user preferences pixel, column, whole matrix or a few columns
                self.load_frames(raw_data, matrix=True, background=True)

                # creat hist and save to csv
                current_histo_file_name = filehelper.Get_out_put_histo_path() \
                                          + filehelper.Get_out_put_file_name()

                self.hist_and_peaks_to_csv(current_histo_file_name)

    def hist_and_peaks_to_csv(self, current_file_name):
        """
        :param current_file_name: string
        """
        current_hist_file_name = current_file_name + '_histo.csv '
        current_peaks_info_name = current_file_name + '_peaks.csv '

        hist, bin_edge = np.histogram(self.adc_array, 10)

        indexes = scipy.signal.argrelextrema(
            np.array(hist),
            comparator=np.less
        )
        print('Peaks are: %s' % (indexes[0]))

        bin_peaks_values = np.array(hist).argsort()[-3:][::-1]

        peaks_info = []
        for peak in bin_peaks_values:

            if hist[peak] != 0:
                peaks_info.append((bin_edge[peak], hist[peak]))
                plt.plot(bin_edge[bin_peaks_values], hist[bin_peaks_values], "x")

        hist, bin_edge = np.histogram(self.adc_array, 100)
        stack = np.stack((bin_edge[:-1], hist), axis=1)
        # plt.bar(bin_edge[:-1], hist, width=10)

        # plt.show()

        np.savetxt(current_peaks_info_name, peaks_info, delimiter=',')
        np.savetxt(current_hist_file_name, stack, delimiter=',')

    def plot_frame(self, frame, min, max):

        '''
        Show frame as in ASPIRE OpenViewer (detectors scheme)
        :param frame: int
        :param min: int
        :param max: int

        '''
        plt.subplots(1)
        if min == 0 & max == 0:
            plt.pcolor(frame, edgecolors='k', linewidths=0.2)
        else:
            plt.pcolor(frame, edgecolors='k', linewidths=0.2, vmin=min, vmax=max)

        plt.subplots_adjust(bottom=0.1, left=0.1, right=0.8, top=0.9)
        cax = plt.axes([0.85, 0.1, 0.075, 0.8])
        plt.colorbar(cax=cax)
        plt.show()
