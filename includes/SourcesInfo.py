"""
Source info class (setter and getter)
"""

class SourcesInfo:
    def __init__(self, source=""):
        self._source = source

    @property
    def Source(self):
        return self._source

    @Source.setter
    def Source(self, source_name):
        """
        Set source
        Parameters
        ----------
        source_name : string, data from file
            Required name of source address
        """
        self._source = source_name

    def GetSourceInfo(self):
        """
        :return: array of spectrum peaks
        """

        source_info = {"Pu": [13.6, 17.2, 20.2],
                       "Fe": [5.89, 6.5],
                       "Cu": [8.9]}
        return source_info[self.Source]
